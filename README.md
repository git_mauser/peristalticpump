# Open Source Peristaltic Pump
This documentation is for a student project done at Otago Polytechnic in cooperation with United Scientists based in Dunedin, New Zealand. The goal of this project was to create a modular, 3d printed, Open Source Peristaltic Pump following the Konstructive system developed by United Scientists (https://zenodo.org/record/5194710).

We tried to list every needed part and all of the code (including comments) for easy replication of our project.
The created pump is controlled by an Arduino Uno microcontroller running a Stepper motor via a TMC2130 Stepper driver. Information is displayed on a 20x4 LCD screen and the menu can be navigated using a Push Button Encoder.

When powered up, the pump displays a welcome screen for two seconds, followed by the Menu. The user has the following options to select by turning the Encoder and pushing its button to confirm:

**Pump** makes the pump spin at a desired speed (can be set in ml/min, ul/min or rpm) until cancelled by pushing the Encoder button.   
**Dose** makes the pump spin at a desired speed until a set volume has been pumped or until cancelled by pushing the Encoder button.   
**Speed** makes it possible for the user to change the pumping speed by turning the Rotary Encoder. A doubleclick changes the speed unit (ml/min, µl/min, rpm) and a single click confirms the set value.    
**Volume** makes it possible for the user to change the dosing volume by turning the Rotary Encoder. A doubleclick changes the volume unit (ml, µl, rotations) and a single click confirms the set value.   
**Direction** can change the stepper motor's turning direction (clockwise or anti-clockwise) by turning the Rotary Encoder. A single click confirms the set direction.                
**Calibrate** makes the pump spin for a preset amount of rotations in a preset time (both changeable in Arduino code if desired, defines CALIBR_ROTATIONS and CALIBR_DURATION in pump.h).             
**Summary** displays the currently set pump variables (Speed, Volume, Direction, CalibrationValue, SpeedUnit, VolumeUnit) on the LCD, scrolling through the menu is possible by rotating the Encoder, pushing it at any point returns to the Main Menu.           
**ResetSett** resets the pump variables mentioned in Summary to 0.     
**SaveSett** saves the pump variables mentioned in Summary to the Arduino's EEPROM storage. Upon next power-up these values will be loaded.


**Bill of Materials**
- Arduino Uno
- TMC2130 Stepper driver
- NEMA17 Stepper Motor (4 pin)
- 20x4 LCD
- Rotary Encoder with Pushbutton
- Breadboard / Perfboard / United Scientists Peristaltic Pump Arduino Shield
- Dupont Wires at least 20cm (x16)
- Stepper Motor cable
- 100µF 25V capacitor
- Male Header Pins (x52)
- Female Header Pins 8pin (x2) (optional, if Stepper driver should be exchangable)
- DC plug and socket (optional, but recommended to provide safe and reliable power connection)
- DC Power Supply 12-24V (If using 12V this can also power the Arduino, otherwise a secondary power source is needed).
- 2 Potentiometers (1k and 10k) (optional, to tune the LCD's brightness and contrast)     

**Tools required**
- FDM 3D Printer
- Soldering Iron
- Multimeter (optional but good to have)
- Screwdrivers / Allen Keys / Power drill for assembly
