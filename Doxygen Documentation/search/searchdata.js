var indexSectionsWithContent =
{
  0: "abcdefilmoprstuvwz",
  1: "ept",
  2: "cdlmoprstw",
  3: "acdefilmopstvz",
  4: "dev",
  5: "abcdimru",
  6: "cdelmprstv"
};

var indexSectionNames =
{
  0: "all",
  1: "files",
  2: "functions",
  3: "variables",
  4: "enums",
  5: "enumvalues",
  6: "defines"
};

var indexSectionLabels =
{
  0: "All",
  1: "Files",
  2: "Functions",
  3: "Variables",
  4: "Enumerations",
  5: "Enumerator",
  6: "Macros"
};

