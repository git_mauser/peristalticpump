/*
   Open Source Peristaltic Pump - Arduino Sketch

    Created on: Oct 30, 2021
        Author: Julian Knaf           

   The goal of this project was the creation of a modular, open source, 3d printable Peristaltic
   Pump. It is controlled with an Arduino Uno powering a TMC2130 Stepper driver, a 20x4 LCD screen
   and a rotary encoder. An Arduino Shield PCB has been developed in order to make the build easier
   and quicker.
   This file includes the main loops of the program and the general menu logic to drive
   the Selection Menu shown on the LCD.
   The Menu was inspired by https://github.com/CarlosSiles67/LCD_Menu/blob/master/LCD_Menu.ino
   In order to make this program run the LiquidCrystal Library should be installed from the
   Arduino Library Manager. Please also check the other tabs for any additional libraries which
   should be installed prior to uploading to your Arduino.
*/
#include <Arduino.h>
#include <LiquidCrystal.h>
#include "TMCStepperControl.h"
#include "eepromstore.h"
#include "encoder.h"
#include "pump.h"
#include "types.h"


//================================DEFINES =========================================================
//LCD connection pins (for Arduino Shield)
#define LCD_RS_PIN 14
#define LCD_EN_PIN 15
#define LCD_D4_PIN 19
#define LCD_D5_PIN 18
#define LCD_D6_PIN 17
#define LCD_D7_PIN 16
#define LCD_ROWS 4
#define LCD_COLUMNS 20

//Define Main menu and sub menus
#define MainMenu          0    //MainMenu
#define PumpScreen        1    //PumpScreen
#define DoseScreen        2    //DoseScreen
#define CalibrationScreen 6    //CalibrationScreen
#define SpeedScreen       3    //pumpSpeed
#define VolumeScreen      4    //pumpVolume
#define DirectionScreen   5    //Direction
#define SummaryScreen     7    //Summary
#define ResetSett
#define SaveScreen        9    //SaveSettings




//===============================GLOBAL VARIABLES=================================================
// Change the Selection Symbol if desired. This is the symbol in front of the selected line of the menu.
const char SelectionSymbol = '>';

int cursorLine = 1;             //The currenly selected line (selectionSymbol is being displayed here)
int displayFirstLine = 1;       //The first line displayed on the display
const int itemsPerScreen = 3;   //amount of menu items displayed on the screen
//(one less than the display's lines)
const int maxItemSize = 11;     //longest menu item text possible(includes char \0)
int menuItems;                  //amount of menu items in the main menu
int summaryItems;               //amount of menu items in the summary
char *menuSelected = NULL;      //pointer to the selected menu option's text
int menuOption = 0;             //integer representing the selected menu option (the defines above can
//also be used in order to make the code more readable. The compiler will
//then change it to the defined integer value.

//These handlers are flags showing whether the respective variable is currently being handled within the
//menu. While in an action menu where the variable is changeable, its handler flag turns to true.
bool volumeHandler = false;
bool speedHandler = false;
bool calibrationHandler = false;
bool directionHandler = false;

//Define Menu Arrays
char startMenu[][maxItemSize] = {"Pump", "Dose", "Speed", "Volume", "Direction", "Calibrate", "Summary", "ResetSett", "SaveSett"};
char SummaryOrder[][maxItemSize] = {"Speed", "Volume", "Direction", "Calibrate", "SpeedUnit", "VolumeUnit"};
unsigned long Summary[] = {pumpSpeed, pumpVolume, pumpDirection, calibrationValue, speedUnit, volumeUnit};   //contains all selected values to be displayed in the Summary Screen


//========================END GLOBAL VARIABLES ====================================================
//Initialise the LCD object with the defined Pins
LiquidCrystal lcd(LCD_RS_PIN, LCD_EN_PIN, LCD_D4_PIN, LCD_D5_PIN, LCD_D6_PIN, LCD_D7_PIN);
extern TMC2130Stepper driver;

//States that the pump can be in - if all are false the pump is in idle
bool pumping = false;
bool dosing = false;
bool calibrating = false;

//SETUP===================================================================================================================
//========================================================================================================================
void setup ()
{
  //Start Serial connection
  Serial.begin(9600);
  while (!Serial);
  Serial.println("Starting...");

  //Start LCD connection and setup special characters
  lcd.begin (LCD_COLUMNS, LCD_ROWS);
  lcd.createChar(0, zero);
  lcd.createChar(1, one);
  lcd.createChar(2, two);
  lcd.createChar(3, three);
  lcd.createChar(4, four);
  lcd.createChar(5, five);
  lcd.createChar(6, ClockwiseChar);
  lcd.createChar(7, AntiClockwiseChar);
  setupMenu();
  setupStepper();   //this function is in TMCStepperControl.h
  restoreSettings();
  display_menu(menuSelected, menuItems, maxItemSize);

  //Initialise Encoder
  encoder = new ClickEncoder(ENCODER_PIN_A, ENCODER_PIN_B, ENCODER_BTN, STEPS_PER_NOTCH);
  Timer1.initialize(1000);
  Timer1.attachInterrupt(timerIsr);
  last = -1;
  encoder->setAccelerationEnabled(true);

  //Print welcome screen, leave message for 2 seconds and clear screen
  welcome();
  delay(2000);
  lcd.clear();
}//end of setup



//LOOP====================================================================================================================
//========================================================================================================================
void loop ()
{
  //Check Encoder Action
  ENCODER_STATE EncoderState = readEncoder();
  switch (EncoderState) {
    //=============================Encoder turned Anti-Clockwise==================================
    case Decreasing:
      //Decreases pumpVolume and prints it
      if (volumeHandler == true) {
        clearLastLine();
        pumpVolume--;
        lcd.setCursor(0, 3);
        lcd.print(pumpVolume);
        lcd.setCursor(5, 3);
        printVolumeUnit();
      }
      //Decreases pumpSpeed and prints it
      else if (speedHandler == true) {
        clearLastLine();
        pumpSpeed--;
        lcd.setCursor(0, 3);
        lcd.print(pumpSpeed);
        lcd.setCursor(5, 3);
        printSpeedUnit();
      }
      //Decreases calibrationValue and prints it
      else if (calibrationHandler == true) {
        clearLastLine();
        calibrationValue -= 1;
        printCalibrationValue();
      }
      //Toggles pumpDirection and prints it
      else if (directionHandler == true) {
        //toggle pumpDirection variable and direction on driver
        pumpDirection++;
        driver.shaft(!driver.shaft());
        printPumpDirection();
        menuOption = MainMenu;
      }
      //Decreases selected menu item (moves up the cursor)
      else {
        moveUp();
        printMenu();
      }
      break;
    //=============================Encoder turned Clockwise==================================
    case Increasing:
      //Increases pumpVolume and prints it
      if (volumeHandler == true) {     //only used to increase or decrease volume
        clearLastLine();
        pumpVolume++;
        lcd.setCursor(0, 3);
        lcd.print(pumpVolume);
        lcd.setCursor(5, 3);
        printVolumeUnit();
      }
      //Increases pumpSpeed and prints it
      else if (speedHandler == true) {
        clearLastLine();
        pumpSpeed++;
        lcd.setCursor(0, 3);
        lcd.print(pumpSpeed);
        lcd.setCursor(5, 3);
        printSpeedUnit();
      }
      //Increases calibrationValue and prints it
      else if (calibrationHandler == true) {
        clearLastLine();
        calibrationValue += 1;
        lcd.setCursor(0, 3);
        printCalibrationValue();
      }
      //Toggles pumpDirection and prints it
      else if (directionHandler == true) {
        pumpDirection++;
        driver.shaft(!driver.shaft());
        printPumpDirection();
        menuOption = MainMenu;

      }
      //Increases the selected menu item (moves cursor down)
      else {
        moveDown();
        printMenu();
      }
      break;
    //=============================Encoder Button pressed==================================
    case ButtonClicked:
      //print the selected submenu
      if (menuOption == MainMenu) {
        selectionMainMenu();
        step_counter = 0;
      }
      //if clicked on summary screen - return to main menu
      else if (menuOption == SummaryScreen)
      {
        returnToMainMenu();
      }
      //otherwise print the menu (update currently printed menu)
      if (volumeHandler == false && speedHandler == false && calibrationHandler == false &&
          pumping == false && dosing == false && directionHandler == false) {
        printMenu();
      }
      break;
    //=============================Encoder Button doublepressed==================================
    case ButtonDoubleClicked:
      //Doubleclick of button in Speed Selection -> change speed unit
      if (speedHandler == true) {
        speedUnit++;
        lcd.setCursor(5, 3);
        printSpeedUnit();
      }
      //Doubleclick of button in Volume Selection-> change volume unit
      else if (volumeHandler == true) {
        volumeUnit++;
        lcd.setCursor(5, 3);
        printVolumeUnit();
      }
      break;
    //=============================Nothing happened (Encoder Idle)==================================
    case Idle:
      //if the pump is in pumping state, move stepper motor by 1 step with the desired
      //delay (call pump() once).
      if (pumping == true) {
        pump(delay_us);
      }
      //if the pump is in dosing state, move the stepper motor by 1 step with the desired
      //delay and draw the progress bar so long the dosing volume has not been reached.
      //Otherwise exit this state and return to the main menu
      else if (dosing == true) {
        //if dosing operation finished (dose() returns true), return to main menu
        if (dose(steps, delay_us, step_counter)) {
          Serial.println(F("Dosing complete"));
          dosing = false;
          step_counter = 0;
          returnToMainMenu();
          printMenu();
        }
        //otherwise update the progress bar of the dosing operation
        //NOTE: This is disabled by default since drawProgressBar uses so much CPU power the
        //delay time between steps is actually increased significantly!
        else {
            //drawProgressBar(step_counter, steps);
        }
      }
      //if the pump is in calibration state, move the stepper motor by 1 step with the desired
      //delay so long the dosing volume has not been reached. Otherwise exit this state and
      //return to the main menu
      else if (calibrating == true) {
        if (dose(CALIBR_STEPS, CALIBR_DELAY_US, step_counter)) {
          Serial.println(F("Calibration complete"));
          calibrating = false;
          returnToMainMenu();
        }
      }
      break;
      //delay(100);
  }

}//end loop()

//========================================FUNCTIONS==============================================================/

/** void welcome() displays a welcome message on the connected LCD.
*/
void welcome()
{
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print(F(" United Scientists  "));
  lcd.setCursor(0, 2);
  lcd.print(F("   Peristaltic Pump  "));
  lcd.setCursor(0, 3);
  lcd.print(F("   Version 1.0  "));
}//end welcome

/** void display_menu(const char *menuInput, int ROWS, int COLS) displays a selected menu on the
    connected LCD. ROWS and COLS can be fixed parameters in this program (menuItems, maxItemSize)
    @param menuInput the selected menu to be displayed on the LCD
    @param ROWS the amount of rows of items to be displayed on the screen
    @param COLS the amount of columns to be displayed on the screen
*/
void display_menu(const char *menuInput, int ROWS, int COLS)
{
  int n = 4;     //4 rows
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print(F("        Menu        "));

  if (ROWS < n - 1) {
    n = ROWS + 1;
  }

  for (int i = 0; i < n - 1; i++) {
    lcd.setCursor(1, i + 1); //(col, row)
    for (int j = 0; j < COLS; j++) {
      if (*(menuInput + ((displayFirstLine + i - 1) * COLS + j)) != '\0') {
        lcd.print(*(menuInput + ((displayFirstLine + i - 1) * COLS + j)));
      }//end if
    }//end for j
  }//end for i

  lcd.setCursor(0, (cursorLine - displayFirstLine) + 1);
  lcd.print(SelectionSymbol);
}//end display_menu

/* void printMenu() prints (updates) the menu selected. It also distinguishes
   between displaying the Summary Screen or any other selected menu.
*/
void printMenu()
{
  // Check in which menu we are
  if (menuOption == SummaryScreen)
  {
    display_summary();
  } else {
    display_menu(menuSelected, menuItems, maxItemSize);
  }
}//end printMenu

/** void selectionMainMenu() shows the submenus after selecting an option within the Main menu, or
    reacts on changed values and saves them in Summary.
    Depending on the selection and the status of the pump the appropriate actions will take place.
*/
void selectionMainMenu()
{
  //Save newly set pumpSpeed in Summary and reset speedHandler
  if (speedHandler == true) {
    Summary[0] = pumpSpeed;     //Sets the speed
    Summary[4] = speedUnit;     //Sets the speedUnit
    speedHandler = false;
    //pumpSpeed = 0;
    returnToMainMenu();
  }
  //Save newly set pumpVolume in Summary and reset volumeHandler
  else if (volumeHandler == true) {
    Summary[1] = pumpVolume;     //Sets the volume
    Summary[5] = volumeUnit;     //Sets the volumeUnit
    volumeHandler = false;
    //pumpVolume = 0;
    returnToMainMenu();
  }
  //Save newly set pumpDirection in Summary and reset directionHandler
  else if (directionHandler == true) {
    Summary[2] = pumpDirection;
    directionHandler = false;
    //pumpDirection = 0;
    returnToMainMenu();
  }
  //Save newly set calibrationValue in Summary and reset calibraitonHandler
  else if (calibrationHandler == true) {
    Summary[3] = calibrationValue;   //Sets the calibrationValue
    calibrationHandler = false;
    calibrating = false;
    //calibrationValue = 0;
    returnToMainMenu();
  }
  //Stops the pumping-state and returns to the Main Menu
  else if (pumping == true) {
    pumping = false;
    digitalWrite(EN_PIN, HIGH);  // Disable driver in hardware

    returnToMainMenu();
  }
  //Stops the dosing-state and returns to the Main Menu
  else if (dosing == true) {
    dosing = false;
    digitalWrite(EN_PIN, HIGH);  // Disable driver in hardware
    returnToMainMenu();
  }
  else if (directionHandler == true) {
    directionHandler = false;
    digitalWrite(EN_PIN, HIGH);  // Disable driver in hardware
    returnToMainMenu();
  }
  //Any other states are treated here
  else {
    lcd.clear();
    //Check which line of the Main Menu has been selected.
    switch (cursorLine - 1)
    {
      case 0:
        //pumping Menu
        displayFirstLine = 1;
        cursorLine = 1;
        lcd.setCursor(0, 0);
        lcd.print(F("PUMPING... "));
        lcd.setCursor(0, 3);
        lcd.print(F("Click to abort"));
        pumping = true;
        Serial.println(F("Pumping started"));
        digitalWrite(EN_PIN, LOW);  // Enable driver in hardware

        //Calculate the values needed for pumping operation
        delay_us = delay_us_calc(pumpSpeed, speedUnit, calibrationValue);
        break;
      case 1:
        displayFirstLine = 1;
        cursorLine = 1;
        lcd.setCursor(0, 0);
        lcd.print(F("DOSING..."));
        lcd.setCursor(0, 3);
        lcd.print(F("Click to abort"));
        Serial.println(F("Dosing started"));
        dosing = true;
        pumping = false;
        digitalWrite(EN_PIN, LOW);  // Enable driver in hardware

        //Calculate the values needed for dosing operation
        steps = steps_calc(pumpVolume, volumeUnit, calibrationValue);
        delay_us = delay_us_calc(pumpSpeed, speedUnit, calibrationValue);
        break;
      case 2:
        //pumpSpeed Menu
        displayFirstLine = 1;
        cursorLine = 1;
        lcd.setCursor(0, 0);
        lcd.print(F("Please set the "));
        lcd.setCursor(0, 1);
        lcd.print(F("Speed and click"));
        lcd.setCursor(0, 2);
        lcd.print(F("Doubleclick for unit"));
        speedHandler = true;
        //pumpSpeed = Summary[0];
        //speedUnit = Summary[4];
        lcd.setCursor(0, 3);
        lcd.print(pumpSpeed);
        lcd.setCursor(5, 3);
        printSpeedUnit();
        break;
      case 3:
        displayFirstLine = 1;
        cursorLine = 1;
        lcd.setCursor(0, 0);
        lcd.print(F("Please set the "));
        lcd.setCursor(0, 1);
        lcd.print(F("Volume and click"));
        lcd.setCursor(0, 2);
        lcd.print(F("Doubleclick for unit"));
        volumeHandler = true;
        //pumpVolume = Summary[1];
        //volumeUnit = Summary[5];
        lcd.setCursor(0, 3);
        lcd.print(pumpVolume);
        lcd.setCursor(5, 3);
        printVolumeUnit();
        break;
      case 4:
        displayFirstLine = 1;
        cursorLine = 1;
        //menuItems = sizeof DirectionContents / sizeof * DirectionContents;
        //menuSelected = &DirectionContents[0][0];
        lcd.setCursor(0, 0);
        lcd.print(F("Please set the"));
        lcd.setCursor(0, 1);
        lcd.print(F("Direction and click"));
        directionHandler = true;
        printPumpDirection();
        menuOption = DirectionScreen;
        break;
      case 5:
        displayFirstLine = 1;
        cursorLine = 1;
        lcd.print(F("CALIBRATING, WAIT..."));
        lcd.setCursor(0, 1);
        lcd.print(F("Set weight of Liquid"));
        lcd.setCursor(0, 2);
        lcd.print(F("when pumping stops"));
        calibrationHandler = true;
        calibrating = true;
        digitalWrite(EN_PIN, LOW);  // Enable driver in hardware
        printCalibrationValue();
        break;
      case 6:
        displayFirstLine = 1;
        cursorLine = 1;
        menuItems = sizeof Summary / sizeof * Summary;
        menuOption = SummaryScreen;
        break;
      case 7:
        displayFirstLine = 1;
        cursorLine = 1;
        resetSettings();
        returnToMainMenu();
        break;
      case 8:
        displayFirstLine = 1;
        cursorLine = 1;
        saveSettings();
        returnToMainMenu();
        break;
    }//end switch
  }//end else
}//end selectionMainMenu

/**

*/
void display_summary()
{
  int n = 4;      // 4 rows in LCD
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print(F("  Settings Summary  "));

  if (summaryItems < n - 1) {
    n = summaryItems + 1;
  }

  for (int i = 1; i < n; i++)
  {
    //Print SummaryOrder[] (names of items like "Speed", "Volume" etc.
    lcd.setCursor(1, i);   //(col, row)
    lcd.print(SummaryOrder[displayFirstLine + i - 2]);

    //Print values
    lcd.setCursor(15, i);
    switch (displayFirstLine + i - 2) {
      case 2:   //Direction printed -> print CW/ACW arrow
        lcd.write((Summary[displayFirstLine + i - 2] == 0) ? byte(6) : byte(7) );
        break;
      case 4:   //SpeedUnit printed -> print unit instead of enum index
        lcd.setCursor(13, i);
        printSpeedUnit();
        break;
      case 5:   //VolumeUnit printed -> print unit instead of enum index
        lcd.setCursor(13, i);
        printVolumeUnit();
        break;
      default:  //Otherwise print the value saved in Summary[]
        lcd.print(Summary[displayFirstLine + i - 2]);
        break;
    }
  }
  //Print SelectionSymbol
  lcd.setCursor(0, (cursorLine - displayFirstLine) + 1);
  lcd.print(SelectionSymbol);

}//end display_summary

/** void printSpeedUnit() prints the variable "speedUnit" according to its current state on the attached
    LCD.
*/
void printSpeedUnit() {
  //lcd.setCursor(5, 3);
  switch (speedUnit) {
    case 0: //UL
      lcd.print(F("ul/min "));
      break;
    case 1: //ML
      lcd.print(F("ml/min "));
      break;
    case 2: //ROT
      lcd.print(F("rot/min"));
      break;
  }
}

/** void printVolumeUnit() prints the variable "volumeUnit" according to its current state on the attached
    LCD.
*/
void printVolumeUnit() {
  //lcd.setCursor(5, 3);
  switch (volumeUnit) {
    case 0: //UL
      lcd.print(F("ul "));
      break;
    case 1: //ML
      lcd.print(F("ml "));
      break;
    case 2: //ROT
      lcd.print(F("rot"));
      break;
  }
}

/** void printPumpDirection() prints the variable "pumpDirection" according to its current state on the
    attached LCD. It also includes a special character in the end of the line depicting the rotational direction
    of the pump head.
*/
void printPumpDirection() {
  lcd.setCursor(0, 3);
  if (pumpDirection == 0) {
    lcd.print(F("Clockwise          "));
    lcd.write(byte(6));
  }
  else if (pumpDirection == 1) {
    lcd.print(F("Anti-Clockwise     "));
    lcd.write(byte(7));
  }
}

/** void printCalibrationValue() prints the variable calibrationValue in grams, since this value is stored as grams
    with the last two digits being after the decimal point.
*/
void printCalibrationValue() {
  lcd.setCursor(0, 3);
  //print grams
  lcd.print((calibrationValue - (calibrationValue % 100)) / 100);
  lcd.print(F("."));
  //print anything after the decimal
  lcd.print(calibrationValue % 100);
  lcd.print(F("g"));
}

/** void clearLastLine() clears the last line of the attached LCD completely and resets the cursor to the start
    of the last line.
*/
void clearLastLine() {
  lcd.setCursor(0, 3);
  lcd.print("                    ");
  lcd.setCursor(0, 3);
}


//MENU FUNCTIONS ========================================================================
/** void setupMenu() calculates the amount of items to be displayed on the screen and sets the
    variables menuSelected and menuOption to the appropriate value.
*/
void setupMenu() {
  menuItems = sizeof startMenu / sizeof * startMenu;
  summaryItems = sizeof Summary / sizeof * Summary;
  menuSelected = &startMenu[0][0]; //Start Menu
  menuOption = MainMenu; //Main Menu
}

/** void moveDown() scrolls down within the selected menu. If the bottom has been
    reached it rolls over to the first element.
*/
void moveDown()
{
  if (cursorLine == (displayFirstLine + itemsPerScreen - 1)) {
    displayFirstLine++;
  }

  //If reached last item roll over to first item
  if (cursorLine == menuItems) {
    cursorLine = 1;
    displayFirstLine = 1;
  } else {
    cursorLine = cursorLine + 1;
  }

}//end moveDown

/** void moveUp() scrolls up within the selected menu. If the top has been reached
    it rolls over to the last element.
*/
void moveUp()
{

  if ((displayFirstLine == 1) & (cursorLine == 1)) {
    if (menuItems > itemsPerScreen - 1) {
      displayFirstLine = menuItems - itemsPerScreen + 1;
    }
  } else if (displayFirstLine == cursorLine) {
    displayFirstLine--;
  }

  if (cursorLine == 1) {
    if (menuItems > itemsPerScreen - 1) {
      cursorLine = menuItems; //roll over to last item
    }
  } else {
    cursorLine = cursorLine - 1;
  }

}//end moveUp

/* void resetSummary() resets all values saved within Summary[] to 0.
*/
void resetSummary()
{
  for (int i = 0; i < sizeof(Summary) / maxItemSize + 1; i++)
  {
    Summary[i] = 0;
  }
}//end resetSummary

/** void returnToMainMenu() resets the menu and draws the main menu.
*/
void returnToMainMenu()
{
  displayFirstLine = 1;
  cursorLine = 1;
  menuItems = sizeof startMenu / sizeof * startMenu;
  menuSelected = &startMenu[0][0];
  menuOption = MainMenu;
}//end returnToMainMenu


/** void drawProgressBar (unsigned long count unsigned long maxCount) draws a progress bar on the
    LCD's second to last row.
    Inspiration from: https://www.instructables.com/Simple-Progress-Bar-for-Arduino-and-LCD/
    @param count the current count of the progress
    @param maxCount maximum value of the progress
*/
void drawProgressBar (unsigned long count, unsigned long maxCount)
{
  //Determine the amount of steps displayed in one column of the bar. The 20x4 LCD has 100 columns
  //(20 characters with 5 columns each).
  double factor = maxCount / ((double)(5 * LCD_COLUMNS));
  //determine how many percent of the operations have passed, and which column on the bar it represents
  int percent = (count + 1) / factor;
  int number = percent / 5;   //where should the next line be drawn?
  int remainder = percent % 5; //amount of columns within one character
  //
  if (number > 0)
  {
    lcd.setCursor(number - 1, LCD_ROWS - 2);
    lcd.write(byte(5));
  }

  lcd.setCursor(number, LCD_ROWS - 2);
  lcd.write(byte(remainder));
}
