/*
   pump.cpp

    Created on: Oct 30, 2021
        Author: Julian Knaf
   This file includes all important variable and function declarations for the
   pump to operate properly. It also includes several functions used to set up the
   TMC2130 Stepper driver in the desired Microstepping mode for higher accuracy or torque.
   Some pump functions needed to excluded from this file because the Arduino would not run
   properly otherwise and not enter the setup()-loop.
*/
#include <Arduino.h>
#include "pump.h"

//====================================================VARIABLES====================================================
//Microstep value - change here for more precise pumping
//see types.h for possible values
//MICRO_STEPS Microsteps = _16;

//============= Changeable values in the Menu =============
unsigned long pumpSpeed = 80;
volUnit speedUnit = ML;
unsigned long pumpVolume = 10;
volUnit volumeUnit = ML;
unsigned long calibrationValue = 2197;
directionUnit pumpDirection = Clockwise;

//============= General values, changeable in code if desired =============
const unsigned long CALIBR_STEPS = CALIBR_ROTATIONS * STEPS_PER_FULL_ROT * MICROSTEPS;
const unsigned long CALIBR_DELAY_US = (CALIBR_DURATION * MICROSEC_PER_SEC) / (CALIBR_STEPS * 2);

//============= Variables calculated during run-time for any operation =============
unsigned long steps;
unsigned long delay_us;
unsigned long step_counter;


//================================= PUMP RELATED FUNCTION DECLARATIONS =================================
/** void pump(unsigned int delay_us) makes the motor move one step further.
    STEP_PIN is set to HIGH for a configurable time _delay_us.
    @param _delay_us the delay time, this parameter sets the speed of the motor, calculated in
                     delay_us_calc().
*/
void pump(unsigned long delay_us) {
  digitalWrite(STEP_PIN, HIGH);
  delayMicroseconds(delay_us);
  digitalWrite(STEP_PIN, LOW);
  delayMicroseconds(delay_us);
}

/** boolean dose(long _steps, int_delay_us, long &inc) moves the motor one step further if the
    needed steps for a dosing command are not reached yet.
    @param _steps the amount of steps needed to perform this dosing operation.
    @param _delay_us the delay time, this parameter sets the speed of the motor, calculated in
                     delay_us_calc().
    @param inc the amount of steps moved in this dosing operation (call by reference, so this
               value can also be read from outside of the function)
    @return true if the dosing operation is finished
            false if the dosing operation is still ongoing.
*/
boolean dose(unsigned long steps, unsigned long delay_us, unsigned long &inc) {
  if (inc < steps) {
    digitalWrite(STEP_PIN, HIGH);
    delayMicroseconds(delay_us);
    digitalWrite(STEP_PIN, LOW);
    delayMicroseconds(delay_us);
    inc++;
    return false;
  }
  else {
    return true;
  }
}

/** long steps_calc(unsigned long volume, volUnit volumeUnit, unsigned long calibrationValue) calculates the needed
    steps for a pumping operation in either mode (rotations, µL or mL).
    @pram volume the desired volume/rotations to pump
    @param unit_mode the mode in which the pumping operation is performed (saved in
                     menu[2].value, 0=mL, 1=µL, 2=rot)
    @param calibrationValue the calibration factor, depends on pump construction, value contains mL/10rot
    @return _steps the needed steps to pump a desired volume
*/
unsigned long steps_calc(unsigned long volume, volUnit volumeUnit, unsigned long calibrationValue) {

  unsigned long steps;
  int decimal_corr;
  double conv; // rotations/volume
  double cal; //volume/rotation

  cal = calibrationValue;
  cal = ((cal / CALIBR_ROTATIONS) / 1000 );


  //apply necessary factor for volumeUnit
  switch (volumeUnit) {
    case 0: //volumeUnit = ul
      conv = 1.0 / cal / 1000;
      break;
    case 1: //volumeUnit = ml
      conv = 1.0 / cal;
      break;
    case 2: //volumeUnit = rot
      conv = 1.0;
  }
  if (volumeUnit == UL) {
    steps = STEPS_PER_FULL_ROT * MICROSTEPS * conv * volume;
  }
  else {
    steps = STEPS_PER_FULL_ROT * MICROSTEPS * conv * volume / 10;
  }

  return steps;
}
/** long delay_us_calc(unsigned long pumpSpeed, volumeUnit volUnit, unsigned long calibrationValue)
    calculates the needed delay between steps (or the motor speed) in oder to get a desired flow rate(or rot/min).
    @param pumpSpeed the desired flow rate of the pump (in µL/min, mL/min or rot/min)
    @param volUnit the mode in which the pumping operation is performed (see types.h)
    @param calibrationValue the calibration factor, depends on pump construction, value contains mL/10rot
    @return _delay_us the needed delay to achieve required flow rate
*/
unsigned long delay_us_calc(unsigned long pumpSpeed, volUnit spdUnit, unsigned long calibrationValue) {

  double d_delay_us;
  unsigned long delay_us;
  int decimal_corr;
  double conv; // rotations/volume
  double cal;


  cal = calibrationValue;
  cal = ((cal / CALIBR_ROTATIONS) / 1000);
  switch (volumeUnit) {
    case 0: //volumeUnit = ul
      conv = 1.0 / cal / 1000;
      break;
    case 1: //volumeUnit = ml
      conv = 1.0 / cal;
      break;
    case 2: //volumeUnit = rot
      conv = 1.0;
  }

  if (speedUnit == UL) {    //= 0
    d_delay_us = (1 / (STEPS_PER_FULL_ROT * MICROSTEPS * conv * pumpSpeed)) * 60 * MICROSEC_PER_SEC / 2;
  }
  else {
    d_delay_us = (1 / (STEPS_PER_FULL_ROT * MICROSTEPS * conv * pumpSpeed / 10)) * 60 * MICROSEC_PER_SEC / 2;

  }

  delay_us = d_delay_us;
  return delay_us;
}
