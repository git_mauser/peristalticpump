/*
 * pump.h
 *
 *  Created on: Oct 30, 2021
 *      Author: Julian Knaf
 * This file includes all important defines and variable definitions to run the pump and stepper
 * driver, and additionally function definitions for the TMC stepper driver to run and be set up 
 * properly. The SPI.h-Library is needed for this to work, but it is a default Arduino library, so no
 * installation is required.
 */
#pragma once
#ifndef PUMP_H
#define PUMP_H
#include <Arduino.h>
#include <SPI.h>
#include "TMCStepperControl.h"
#include "types.h"


//Defines for Pump
#define MICROSEC_PER_SEC 1000000
#define CALIBR_ROTATIONS 30
#define CALIBR_DURATION 30 // seconds
//#define CALIBR_DECIMALS 2
#define STEPS_PER_FULL_ROT 200


//============= Changeable values in the Menu =============
extern unsigned long pumpSpeed;
extern volUnit speedUnit;
extern unsigned long pumpVolume;
extern volUnit volumeUnit;
extern unsigned long calibrationValue;
extern directionUnit pumpDirection;

//============= General values, changeable in code if desired =============
extern const unsigned long CALIBR_STEPS;
extern const unsigned long CALIBR_DELAY_US;

//============= Variables calculated during run-time for any operation =============
extern unsigned long steps;
extern unsigned long delay_us;
extern unsigned long step_counter;

//FUNCTION DEFINITIONS==========================================
unsigned long steps_calc(unsigned long volume, volUnit volumeUnit, unsigned long calibrationValue);
unsigned long delay_us_calc(unsigned long pumpSpeed, volUnit spdUnit, unsigned long calibrationValue);
void pump(unsigned long delay_us);
boolean dose(unsigned long steps, unsigned long delay_us, unsigned long &inc);



#endif
