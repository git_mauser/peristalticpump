/*
 * types.h
 *
 *  Created on: Oct 30, 2021
 *      Author: Julian Knaf
 * This file includes all variable and function definitions for special data types created for this pump.
 * Multiple Enums and special characters for the LCD are defined here.
 * The three functions used in this file are overloaded ++ -operators which make it possible
 * to intuitively increment the custom datatypes defined within this file.
 */
#pragma once
#ifndef TYPES_H
#define TYPES_H
#include <Arduino.h>


//========================================== SPECIAL CHARACTERS ==========================================
extern byte zero[];
extern byte one[];
extern byte two[];
extern byte three[];
extern byte four[];
extern byte five[];
extern byte ClockwiseChar[];
extern byte AntiClockwiseChar[];

//============================================= ENUMERATIONS =============================================
//represents all possible Encoder states
enum ENCODER_STATE {
  Decreasing,
  Increasing,
  ButtonPressed,
  ButtonHeld,
  ButtonReleased,
  ButtonClicked,
  ButtonDoubleClicked,
  Idle
};
//the possible volume units within the pump
enum volUnit {
  UL,
  ML,
  ROT
};
//possible directions to run the pump at
enum directionUnit {
  Clockwise,
  AntiClockwise
};

//========================================= FUNCTION DEFINITIONS =========================================
extern volUnit& operator++(volUnit& volumeUnit);
extern directionUnit& operator++(directionUnit& dirUnit);

#endif
