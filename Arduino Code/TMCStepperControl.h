/*
   TMCStepperControl.h

    Created on: Oct 30, 2021
        Author: Julian Knaf

    This file includes all important variable and function definitions as well as constants needed for the
    Stepper diver's operation. The driver is interfaced with the TMCStepper.h Library Version 0.7.3
    (https://github.com/teemuatlut/TMCStepper). It can also be downloaded and installed from within the
    Arduino Library Manager.

*/
#pragma once
#ifndef TMCSTEPPERCONTROL_H
#define TMCSTEPPERCONTROL_H

#include <TMCStepper.h>

#define MICROSTEPS       4        // Microstep-Value - 0,2,4,8,16,32,64,128 or 255
#define RMS_CURRENT      800      //mA
#define R_SENSE 0.11f             // SilentStepStick series: 0.11 ; UltiMachine Einsy/Archim2: 0.2
// Panucatt BSD2660: 0.1 ; Watterott TMC5160: 0.075

//NOTE: These Pin values are adapted for the mistake made in the design process of PCB v1.0. v1.1 uses different
//Pins. If using PCB v1.1 please uncomment this line:
//#define USING_PCB_V1_1


#ifndef USING_PCB_V_1_!
const int EN_PIN      =     5;        // Enable
const int DIR_PIN     =     8;        // Direction
const int STEP_PIN    =     9;        // Step
const int CS_PIN      =     10;       // Chip select
const int SW_MOSI     =     7;        // Software Master Out Slave In (MOSI)
const int SW_MISO     =     6;        // Software Master In Slave Out (MISO)
const int SW_SCK      =     11;       // Software Slave Clock (SCK)
#else
const int EN_PIN      =     8;        // Enable
const int DIR_PIN     =     5;        // Direction
const int STEP_PIN    =     7;        // Step
const int CS_PIN      =     6;        // Chip select
const int SW_MOSI     =     9;        // Software Master Out Slave In (MOSI)
const int SW_MISO     =     10;       // Software Master In Slave Out (MISO)
const int SW_SCK      =     11;       // Software Slave Clock (SCK)
#endif



//FUNCTION DEFINITIONS
void setupStepper ();
#endif
