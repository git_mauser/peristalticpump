/*
 * types.cpp
 *
 *  Created on: Oct 30, 2021
 *      Author: Julian Knaf
 */
#include <Arduino.h>
#include "types.h"

//=========================== SPECIAL CHARACTERS ===========================
//This character represents a clockwise arrow within one character of the LCD
static byte ClockwiseChar[] = {
  B01111,
  B00001,
  B00001,
  B01001,
  B11101,
  B01001,
  B01111,
  B00000
};
//This character represents an anti-clockwise arrow within one character of the LCD
static byte AntiClockwiseChar[] = {
  B11110,
  B10000,
  B10000,
  B10010,
  B10111,
  B10010,
  B11110,
  B00000
};

//This character represents an empty character, used for the progress bar
static byte zero[] = {
  B00000,
  B00000,
  B00000,
  B00000,
  B00000,
  B00000,
  B00000,
  B00000
};
//This character represents a character with the first column drawn, used for the
//progress bar
static byte one[] = {
  B10000,
  B10000,
  B10000,
  B10000,
  B10000,
  B10000,
  B10000,
  B10000
};
//This character represents a character with the first two columns drawn, used for the
//progress bar
static byte two[] = {
  B11000,
  B11000,
  B11000,
  B11000,
  B11000,
  B11000,
  B11000,
  B11000
};
//This character represents a character with the first three columns drawn, used for the
//progress bar
static byte three[] = {
  B11100,
  B11100,
  B11100,
  B11100,
  B11100,
  B11100,
  B11100,
  B11100
};
//This character represents a character with the first four columns drawn, used for the
//progress bar
static byte four[] = {
  B11110,
  B11110,
  B11110,
  B11110,
  B11110,
  B11110,
  B11110,
  B11110
};
//This character represents a character with all five columns drawn, used for the
//progress bar
static byte five[] = {
  B11111,
  B11111,
  B11111,
  B11111,
  B11111,
  B11111,
  B11111,
  B11111
};

//=========================== FUNCTION DECLARATIONS ===========================
/** overloaded ++ operator to make it possible to increase variables of the
    type volUnit by just typing "volumeUnit++;" or "speedUnit++;". After the
    variable reaches its maximum value (2) it gets rolled back to 0 by increasing.
    @param volumeUnit a variable of data type volUnit
    @return the increased variable
*/
volUnit& operator++(volUnit& volumeUnit)
{
  switch (volumeUnit) {
    case 0 : return volumeUnit = 1;
    case 1 : return volumeUnit = 2;
    case 2 : return volumeUnit = 0;
  }
}

/** overloaded ++ operator to make it possible to increase variables of the type
    directionUnit by just typing "pumpDirection++;". It is technically no increasing
    but a toggle since direction could also be represented as boolean value.
    @param dirUnit a variable of data type directionUnit
    @return the increased (toggled) variable
*/
directionUnit& operator++(directionUnit& dirUnit)
{
  switch (dirUnit) {
    case 0 : return dirUnit = 1;
    case 1 : return dirUnit = 0;
  }
}
