#pragma once
#ifndef EEPROMSTORE_H
#define EEPROMSTORE_H

#include <Arduino.h>
#include <EEPROM.h>
//#include <avr/eeprom.h>
#include "pump.h"

extern unsigned long Summary[];
extern void resetSummary();

/** int wirteLongArrayToEEPROM(int address, long numbers[] int arraySize) writes a given 
 *  array of long (or any other 4-byte datatype)to a given address in the Arduino's 
 *  EEPROM storage.
 *  @param address the address where the value should be stored (starting address)
 *  @param numbers the array containing the values to be written to EEPROM
 *  @param arraySize the size of the array to be written (number of elements contained)
 */
void writeLongArrayToEEPROM(int address, long numbers[], int arraySize)
{
  int addressIndex = address;
  for (int i = 0; i < arraySize; i++)
  {
    EEPROM.write(addressIndex, (numbers[i] >> 24) & 0xFF);
    EEPROM.write(addressIndex + 1, (numbers[i] >> 16) & 0xFF);
    EEPROM.write(addressIndex + 2, (numbers[i] >> 8) & 0xFF);
    EEPROM.write(addressIndex + 3, numbers[i] & 0xFF);
    addressIndex += 4;
  }
}

/** int readLongArrayFromEEPROM(int address, long numbers[] int arraySize) reads a given 
 *  array of long (or any other 4-byte datatype)from a given address in the Arduino's 
 *  EEPROM storage and fills the Array.
 *  @param address the address where the values are stored (starting address)
 *  @param numbers the array where the read values should be stored in
 *  @param arraySize the size of the array to be written (number of elements contained)
 */
void readLongArrayFromEEPROM(int address, long numbers[], int arraySize)
{
  int addressIndex = address;
  for (int i = 0; i < arraySize; i++)
  {
    numbers[i] = ((long)EEPROM.read(addressIndex) << 24) +
                 ((long)EEPROM.read(addressIndex + 1) << 16) +
                 ((long)EEPROM.read(addressIndex + 2) << 8) +
                 (long)EEPROM.read(addressIndex + 3);
    addressIndex += 4;
  }
}

/** void saveSettings() saves the settings of the pump to EEPROM
*/
inline void saveSettings() {
  //Save the Summary
  writeLongArrayToEEPROM(0,Summary, 6 );
  
}
/** void resetSettings() resets the settings of the pump to 0
*/
inline void resetSettings() {
  pumpVolume = 0;
  pumpSpeed = 0;
  pumpDirection = 0;
  calibrationValue = 0;
  speedUnit = 0;
  volumeUnit = 0;
  resetSummary();
}
/** void restoreSettings() restores the settings of the pump and 
    the Summary to the values stored in EEPROM
*/
inline void restoreSettings() {
  //Read data from EEPROM
  readLongArrayFromEEPROM (0,Summary,6);
  //data from Summary to variables used in operation
  pumpSpeed = Summary[0];
  pumpVolume = Summary[1];
  pumpDirection = Summary[2];
  calibrationValue = Summary[3];
  speedUnit = Summary[4];
  volumeUnit = Summary[5];
}

#endif
