/*
 * encoder.h
 *
 *  Created on: Oct 30, 2021
 *      Author: Julian Knaf
 *      
 * This file includes all important variables, constants and functions needed
 * for the Rotary Encoder's operation. The Encoder is interfaced with the ClickEncoder.h
 * library (found on https://github.com/0xPIT/encoder), the TimerOne-Library also needs
 * to be installed for this to run properly. TimerOne can be installed from within the
 * Arduino Library Manager.
 */
 
#pragma once
#ifndef ENCODER_H
#define ENCODER_H

#include <TimerOne.h>
#include <ClickEncoder.h>
#include "types.h"

// Rotary Encoder Connection Pins (for Arduino Shield)
const byte STEPS_PER_NOTCH = 2;
const byte ENCODER_PIN_A = 2;
const byte ENCODER_PIN_B = 3;
const byte ENCODER_BTN = 4;
//Initialise the ClickEncoder object with the defined Pins
ClickEncoder *encoder;
void timerIsr() {
  encoder->service();
}
//Needed Variables for the operation of the Encoder
int16_t last, value;


/** ENCODER_STATE readEncoder() evaluates the inputs from the connected Rotary
 *  Encoder. It can distinguish between a lot of different states (see types.h)
 *  @return the current state of the Encoder
 */
ENCODER_STATE readEncoder() {
  value += encoder->getValue();
  ENCODER_STATE state;
  //Check whether the value has changed
  if (value != last) {
    //Encoder value decreases
    if (last > value) {
      state = Decreasing;
    }
    //Encoder value increases
    else if (last < value) {
      state = Increasing;
    }
    last = value;
  } else if (value == last) {
    state = Idle;
  }

  //Evaluate Button Input
  ClickEncoder::Button b = encoder->getButton();
  if (b != ClickEncoder::Open) {
    switch (b) {
      case ClickEncoder::Pressed:
        state = ButtonPressed;
        break;

      case ClickEncoder::Held:
        state = ButtonHeld;
        break;

      case ClickEncoder::Released:
        state = ButtonReleased;
        break;

      case ClickEncoder::Clicked:
        state = ButtonClicked;
        break;

      case ClickEncoder::DoubleClicked:
        state = ButtonDoubleClicked;
        break;

      default:
        state = Idle;
        break;
    }
  }
  return state;
}//end readEncoder

#endif
