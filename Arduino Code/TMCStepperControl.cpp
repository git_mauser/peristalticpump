/*
 * TMCStepperControl.cpp
 *
 *  Created on: Oct 30, 2021
 *      Author: Julian Knaf
 *      
 *  This file includes all function definitions needed for the
 *  operation of the TMC2130 Stepper driver.
 *  
 */
 #include "TMCStepperControl.h"

//Creates a TMC2130 Stepper driver object (Part of TMCStepper.h)
TMC2130Stepper driver = TMC2130Stepper(CS_PIN, R_SENSE, SW_MOSI, SW_MISO, SW_SCK); // Software SPI



//=================================FUNCTIONS=================================
/** void setupStepper() initialises the stepper driver to pre-defined modes. The changeable values
 *  can be seen in TMCStepperControl.h.
 */
void setupStepper () {
  //STEPPER==============
  driver.begin();       // Initiate pins and registeries
  driver.rms_current(RMS_CURRENT);  // Set stepper current
  driver.en_pwm_mode(1);    // Enable extremely quiet stepping
  driver.microsteps(MICROSTEPS);
  //Set the EN and STEP pins as output 
  pinMode(EN_PIN, OUTPUT);
  pinMode(STEP_PIN, OUTPUT);
  digitalWrite(EN_PIN, HIGH); //disable the stepper at boot up
}
